<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Realestate-ui</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header" class="Blueheader">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/white-logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about-us.php">About Us</a>
								
							</li>
							<li class="submenu">
								<a href="services.php">Service</a>
								<ul>
									<li>
										<a href="#">Buy a Home</a>				
									</li>
									<li>
										<a href="#">Sell a Home</a>				
									</li>
									<li>
										<a href="#">Rent a Home</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="news.php">News</a>
							</li>
							<li>
								<a href="contact-us.php">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
							<strong>Menu</strong>
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">Home</a>
		</li>
		<li>
			<a href="about-us.php">About Us</a>
		</li>
		<li class="haveSubmenu">
			<a href="our-menu.php">Service</a>
			<ul>
				<li>
					<a href="#">Pizza</a>				
				</li>
				<li>
					<a href="#">Sandwich</a>				
				</li>
				<li>
					<a href="#">French Fries</a>				
				</li>
				<li>
					<a href="#">Cheeseburger</a>				
				</li>
				<li>
					<a href="#">Samosa</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="news.php">News</a>
		</li>
		<li>
			<a href="contact-us.php">Contact</a>
		</li>
	</ul>
</nav>

<main>

<section class="noBanner"></section>

<section class="BreadcrumBlock">
	<div class="container">
		<ul class="breadcrum">
			<li><a href="#">Home</a></li>
			<li><a href="#">Services</a></li>
			<li>Single Services</li>
		</ul>
	</div>
</section>

<section class="PropertyDetails">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-6">
				<div class="SliderBlock">
					<div class="main">
						<div class="slider slider-for">
							<div><img src="assets/img/tempimg/dummy.png" alt=""></div>
							<div><img src="assets/img/tempimg/dummy.png" alt=""></div>
							<div><img src="assets/img/tempimg/dummy.png" alt=""></div>
							<div><img src="assets/img/tempimg/dummy.png" alt=""></div>
							<div><img src="assets/img/tempimg/dummy.png" alt=""></div>
						</div>
						<div class="slider slider-nav">
							<div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
							<div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
							<div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
							<div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
							<div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-6">
				<div class="RightContent">
					<h2>Property Description</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue, consectetur at luctus sit amet, viverra mollis leo.</p>
					<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue, consectetur at luctus sit amet, viverra mollis leo.</p>
					<a href="#test-popup" class="OrangeWhiteBtn open-popup-link">Contact us <span><svg><use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use></svg></span></a>
					<div id="test-popup" class="white-popup mfp-hide">
					  <h4 class="LeftYellowElement">Contact Us</h4>
					  <h2>Fill the form to contact us.</h2>
					  	<form action="">
							<div class="row">
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Full Name">
								</div>
								<div class="col-12 col-md-6">
									<input type="email" placeholder="E-mail">
								</div>
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Phone">
								</div>
								<div class="col-12 col-md-6">
									<input type="text" placeholder="Subject">
								</div>
								<div class="col-12 col-md-12">
									<textarea placeholder="Message"></textarea>
								</div>
								<div class="col-12 col-md-12">
									<div class="submit"><input type="submit" value="Submit"></div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="Section SingleServiceCards ThreeCardsBlock OffWhiteSection">
	<div class="container">
		<div class="TopHeadingSec">
			<h2>Latest Properties</h2>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/service-img.png" alt="">
						<div class="bottomContent">
							<h3>Buy a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/bannerimg.png" alt="">
						<div class="bottomContent">
							<h3>Sell a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/leftimg.png" alt="">
						<div class="bottomContent">
							<h3>Rent a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>








<?php @include('template-parts/footer.php') ?>