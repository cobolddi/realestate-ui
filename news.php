<?php @include('template-parts/header.php') ?>

<section class="InsideBanner">
	<picture>
		<source media="(min-width:650px)" srcset="assets/img/tempimg/insidebanner.svg">
		<source media="(min-width:465px)" srcset="assets/img/tempimg/insidebanner.svg">
		<img src="assets/img/tempimg/mobile-insidebanner.jpg" alt="Z-sites" style="width:auto;">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="content">
				<h1>News</h1>
				<ul>
					<li>Home</li>
					<li><a href="#">News</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>



<section class="Section NewsArticles ThreeCardsBlock">
	<div class="container">
		<div class="LeftHeadingWithLink">
			<div class="LeftHeading">
				<h4 class="LeftYellowElement">News</h4>
				<h2>Latest News & Articles</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			</div>
			<div class="LinkBox">
				<form >
					<input type="search" placeholder="Search">
					<input type="submit" value="">
				</form>
			</div>
		</div>
		<div class="FourCardsBlock">
			<div class="row">
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news1.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news2.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news3.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news4.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news1.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news2.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news3.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
				<div class="col-12 col-sm-4 col-lg-3">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/news4.png" alt="">
						<div class="bottomContent">
							<h6 class="LeftBlueElement">March 8, 2021</h6>
							<h3>How to choose the Perfect Planner</h3>
							<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
							<ul>
								<li>By Admin /</li>
								<li>0 Comments</li>
							</ul>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>





<?php @include('template-parts/footer.php') ?>