<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Realestate-ui</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header" class="Blueheader">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/white-logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about-us.php">About Us</a>
								
							</li>
							<li class="submenu">
								<a href="services.php">Service</a>
								<ul>
									<li>
										<a href="#">Buy a Home</a>				
									</li>
									<li>
										<a href="#">Sell a Home</a>				
									</li>
									<li>
										<a href="#">Rent a Home</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="news.php">News</a>
							</li>
							<li>
								<a href="contact-us.php">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
							<strong>Menu</strong>
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">Home</a>
		</li>
		<li>
			<a href="about-us.php">About Us</a>
		</li>
		<li class="haveSubmenu">
			<a href="our-menu.php">Service</a>
			<ul>
				<li>
					<a href="#">Pizza</a>				
				</li>
				<li>
					<a href="#">Sandwich</a>				
				</li>
				<li>
					<a href="#">French Fries</a>				
				</li>
				<li>
					<a href="#">Cheeseburger</a>				
				</li>
				<li>
					<a href="#">Samosa</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="news.php">News</a>
		</li>
		<li>
			<a href="contact-us.php">Contact</a>
		</li>
	</ul>
</nav>

<main>

<section class="noBanner"></section>

<section class="BreadcrumBlock">
	<div class="container SmallContainer">
		<ul class="breadcrum">
			<li><a href="#">Home</a></li>
			<li><a href="#">News</a></li>
			<li>News details</li>
		</ul>
	</div>
</section>



<section class="SingleContent">
	<div class="container SmallContainer">
		<div class="MainImage">
			<img src="assets/img/tempimg/news-single.png" alt="">
		</div>
		<div class="ContentOnly">
			<h1>Minimalist Style For Good Life</h1>
			<ul class="authorlist">
				<li><a href="#">March 8, 2021 /</a></li>
				<li><a href="#">By Admin /</a></li>
				<li><a href="#">0 Comments</a></li>
			</ul>
			<p>Nulla vel ex lorem. Donec magna ante, porttitor vestibulum ultricies ut, ornare id leo. Duis venenatis, ex in consectetur venenatis, mauris nibh maximus arcu, id pulvinar mi erat quis ante. Cras vel auctor dui. Donec lobortis lorem in bibendum aliquam. Quisque a eros pretium, aliquam risus ac, scelerisque justo. Fusce luctus ante ut ante condimentum, euismod iaculis sem convallis. Suspendisse tincidunt dapibus nunc vitae placerat. </p>
			<p>Morbi id felis quis sem tincidunt cursus. Nam ullamcorper maximus aliquet. Nulla dignissim ex orci, id pulvinar ante finibus cursus. Etiam dictum, mi nec suscipit tincidunt, leo lacus cursus lacus, a dignissim dui massa ac leo. Vivamus maximus sem quis massa tristique, elementum accumsan turpis pretium. Quisque iaculis, purus id imperdiet porta, odio augue sodales turpis, eget scelerisque nunc enim maximus elit.</p>
			<img src="assets/img/tempimg/news-single.png" alt="" class="singleImg">
			<p>Nulla vel ex lorem. Donec magna ante, porttitor vestibulum ultricies ut, ornare id leo. Duis venenatis, ex in consectetur venenatis, mauris nibh maximus arcu, id pulvinar mi erat quis ante. Cras vel auctor dui. Donec lobortis lorem in bibendum aliquam. Quisque a eros pretium, aliquam risus ac, scelerisque justo. Fusce luctus ante ut ante condimentum, euismod iaculis sem convallis. Suspendisse tincidunt dapibus nunc vitae placerat. </p>
			<p>Morbi id felis quis sem tincidunt cursus. Nam ullamcorper maximus aliquet. Nulla dignissim ex orci, id pulvinar ante finibus cursus. Etiam dictum, mi nec suscipit tincidunt, leo lacus cursus lacus, a dignissim dui massa ac leo. Vivamus maximus sem quis massa tristique, elementum accumsan turpis pretium. Quisque iaculis, purus id imperdiet porta, odio augue sodales turpis, eget scelerisque nunc enim maximus elit.</p>

			<div class="ShareButton">
				<p>Share this Article :</p>
				<ul>
					<li><a href="#"><img src="assets/img/twitter-pink.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/linkedin-pink.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/facebook-pink.svg" alt=""></a></li>
					<li><a href="#"><img src="assets/img/insta-pink.svg" alt=""></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>






<?php @include('template-parts/footer.php') ?>