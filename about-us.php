<?php @include('template-parts/header.php') ?>

<?php @include('template-parts/pageHeader/InsideBanner.php') ?>

<?php @include('template-parts/LeftImgRightContent.php') ?>

<?php @include('template-parts/FourCardsBlock.php') ?>

<?php @include('template-parts/TeamCardsBlock.php') ?>

<section class="Section TestimonialsSec OffWhiteSection InsidePageTestimonial">
	<div class="container">
		<h2>Client feedback</h2>
		<div class="testimonialslider">
		  	<div class="SlideBlock">
		  		<div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
		  		<h3>This is due to their excellent service, Competitive pricing and customer support. It’s throughly refreshing to get such a personal touch.</h3>
		  		<p>Uday Handa / <span>Founder</span></p>
		  	</div>
		  	<div class="SlideBlock">
		  		<div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
		  		<h3>This is due to their excellent service, Competitive pricing and customer support. It’s throughly refreshing to get such a personal touch.</h3>
		  		<p>Uday Handa / <span>Founder</span></p>
		  	</div>
		  	<div class="SlideBlock">
		  		<div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
		  		<h3>This is due to their excellent service, Competitive pricing and customer support. It’s throughly refreshing to get such a personal touch.</h3>
		  		<p>Uday Handa / <span>Founder</span></p>
		  	</div>
		</div>
	</div>	
</section>

<?php @include('template-parts/footer.php') ?>