$(document).ready(function() {
  //-----HeaderPadding
  var headerHeight = $("#header").outerHeight(true);
  // console.log("Header Height", headerHeight);
  $("main").css("paddingTop", headerHeight);

  $(function() {
    $("li")
      .has("ul")
      .mouseover(function() {
        $(this)
          .children("ul")
          .css("visibility", "visible");
      })
      .mouseout(function() {
        $(this)
          .children("ul")
          .css("visibility", "hidden");
      });
  });

  // Menu Overlay Animation
  var toggles = document.querySelectorAll(".c-hamburger");

  for (var i = toggles.length - 1; i >= 0; i--) {
    var toggle = toggles[i];
    toggleHandler(toggle);
  }

  function toggleHandler(toggle) {
    toggle.addEventListener("click", function(e) {
      e.preventDefault();
      if (this.classList.contains("is-active") === true) {
        this.classList.remove("is-active");
        $(".open").removeClass("oppenned");
      } else {
        this.classList.add("is-active");
        $(".open").addClass("oppenned");
      }
    });
  }
  $(".sub-menu li a").click(function(event) {
    $(".open").removeClass("oppenned");
    $(".c-hamburger").removeClass("is-active");
  });

  let checkScrennSize = function() {
    if (window.matchMedia("(max-width: 991px)").matches) {
      $(".haveSubmenu>a").append(
        '<span><img src="assets/img/arw-down-white.svg"></span>'
      );
    }
    $(".haveSubmenu")
      .find("a>span")
      .click(function(e) {
        // $(".haveSubmenu>a>span").click(function(e){
        e.stopPropagation();
        e.preventDefault();
        $(this)
          .parents("a")
          .parent()
          .toggleClass("Active_MobileDropdown");
      });
  };
  checkScrennSize();

  // Video play on click functionality
  const videoBtn = document.querySelector(".js-videoPoster");

  const playVideo = () => {
    videoBtn.addEventListener("click", () => {
      const video = document.querySelector(".js-videoIframe");

      if (video.paused) {
        video.play();
        videoBtn.style.opacity = 0;
        video.setAttribute("controls", "true");
      } else {
        video.pause();
        videoBtn.style.opacity = 1;
      }
    });
  };

  if (videoBtn) {
    playVideo();
  }

  // NewsSLider
  $(".newslider").slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          arrows: true,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 400,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  // TestimonialSlider
  $(".testimonialslider").slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [
      {
        breakpoint: 600,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 400,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });

  $(".logosliders").slick({
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
          slidesToShow: 3,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          arrows: false,
          slidesToShow: 2,
          slidesToScroll: 1
        }
      }
    ]
  });

  // Slider Gallery

  $(".slider-for").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: false,
    asNavFor: ".slider-nav"
  });
  $(".slider-nav").slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: ".slider-for",
    dots: false,
    arrows: false,
    focusOnSelect: true
  });

  $(".PlayBtn").magnificPopup({
    type: "iframe",
    midClick: true,
    mainClass: "mfp-with-zoom", // this class is for CSS animation below

    zoom: {
      enabled: true, // By default it's false, so don't forget to enable it

      duration: 300, // duration of the effect, in milliseconds
      easing: "ease-in-out", // CSS transition easing function

      // The "opener" function should return the element from which popup will be zoomed in
      // and to which popup will be scaled down
      // By defailt it looks for an image tag:
      opener: function(openerElement) {
        // openerElement is the element on which popup was initialized, in this case its <a> tag
        // you don't need to add "opener" option if this code matches your needs, it's defailt one.
        return openerElement.is("img")
          ? openerElement
          : openerElement.find("img");
      }
    }
  });

  $(".image-popup-vertical-fit").magnificPopup({
    type: "image",
    mainClass: "mfp-with-zoom",
    gallery: {
      enabled: true
    },

    zoom: {
      enabled: true,

      duration: 300, // duration of the effect, in milliseconds
      easing: "ease-in-out", // CSS transition easing function

      opener: function(openerElement) {
        return openerElement.is("img")
          ? openerElement
          : openerElement.find("img");
      }
    }
  });

  $(".open-popup-link").magnificPopup({
    type: "inline",
    midClick: true,
    mainClass: "mfp-fade"
  });
});

//Accordion
$(".accordion .accordion-item button").click(function() {
  $(this)
    .parent()
    .toggleClass("active");
});

// tabs
$("[data-toggle='tab']").click(function() {
  var tabs = $(this).attr("data-tabs");
  var tab = $(this).attr("data-tab");
  $(".btn-tab").removeClass("active");
  $(this).addClass("active");
  $(tabs)
    .find(".gtab")
    .removeClass("active");
  $(tabs)
    .find(tab)
    .addClass("active");
});
