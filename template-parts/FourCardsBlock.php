<section class="Section FourCardsBlock OffWhiteSection">
	<div class="container">
		<div class="TopHeadingSec">
			<h4 class="LeftYellowElement">Our Values</h4>
			<h2>Why Choose Us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin tincidunt<br> massa id leo tincidunt, vehicula fringilla ante finibus.</p>
		</div>
		<div class="CardsBlock">
			<div class="row">
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="Cards">
						<div class="IconBox">
							<svg><use xlink:href="assets/img/cobold-sprite.svg#transparent"></use></svg>
						</div>
						<div class="ContentBox">
							<h5>Transparency</h5>
							<p>There are many new variations of pasages of available text.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="Cards">
						<div class="IconBox">
							<svg><use xlink:href="assets/img/cobold-sprite.svg#expert"></use></svg>
						</div>
						<div class="ContentBox">
							<h5>Expertise</h5>
							<p>There are many new variations of pasages of available text.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="Cards">
						<div class="IconBox">
							<svg><use xlink:href="assets/img/cobold-sprite.svg#integrity"></use></svg>
						</div>
						<div class="ContentBox">
							<h5>Integrity</h5>
							<p>There are many new variations of pasages of available text.</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-sm-6 col-lg-3">
					<div class="Cards">
						<div class="IconBox">
							<svg><use xlink:href="assets/img/cobold-sprite.svg#respect"></use></svg>
						</div>
						<div class="ContentBox">
							<h5>Respect</h5>
							<p>There are many new variations of pasages of available text.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>