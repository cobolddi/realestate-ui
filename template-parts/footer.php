
</main>
<footer>
	<div class="TopFooter">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-4">
					<div class="logobox">
						<a href="index.php" class="footerlogo"><img src="assets/img/ftr-logo.svg" alt=""></a>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quam magna, pharetra vel pretium a, accumsan eleifend metus.</p>
					</div>
				</div>
				<div class="col-12 col-lg-2">
					<div class="QuickLink">
						<h4>Explore</h4>
						<ul class="links">
							<li><a href="index.php">Home</a></li>
							<li><a href="about-us.php">About Us</a></li>
							<li><a href="services.php">Service</a></li>
							<li><a href="news.php">News</a></li>
							<li><a href="contact-us.php">Contact Us</a></li>
						</ul>
					</div>
				</div>
				<div class="col-12 col-lg-3">
					<div class="QuickLink Addlinks">
						<h4>Conatct Us</h4>
						<ul class="links">
							<li><strong>Add : </strong>Thinkvalley B13, Sector32, Gurgaon - 122002, Haryana (India)</li>
							<li>Email : <a href="mailto:info@abcd.com">info@abcd.com</a></li>
							<li>Phone : <a href="tel:+91 981079XXXX">+91 981079XXXX</a></li>
						</ul>
					</div>

					
					
				</div>
				
				<div class="col-12 col-lg-3">
					<div class="ShareButton">
						<h4>Subscribe to our newsletter</h4>
						<form action="">
							<input type="email" placeholder="Your Email">
							<input type="submit" value="">
						</form>
						<ul>
							<li><a href="#"><img src="assets/img/facebook.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/linkedin.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/twitter.svg" alt=""></a></li>
							<li><a href="#"><img src="assets/img/insta.svg" alt=""></a></li>
						</ul>
					</div>
				</div>

			</div>
		</div>			
	</div>
	<div class="CopyRight">
		<p>All rights reserved. © 2021 zsites.in</p>
	</div>
</footer>

<script src="assets/js/vendor.min.js"></script>
<script src="assets/js/scripts.min.js"></script>
</body>
</html>