<section class="Section ThreeCardsBlock ">
	<div class="container">
		<div class="LeftHeadingWithLink">
			<div class="LeftHeading">
				<h4 class="LeftYellowElement">Services</h4>
				<h2>Whether you’re buying, selling or renting, we can help you move forward.</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br>Pellentesque eu est in est accumsan malesuada at sed ipsum.</p>
			</div>
			<div class="LinkBox">
				<a href="#" class="OrangeWhiteBtn">See All <span><svg><use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use></svg></span></a>
			</div>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/service-img.png" alt="">
						<div class="bottomContent">
							<h3>Buy a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/bannerimg.png" alt="">
						<div class="bottomContent">
							<h3>Sell a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/leftimg.png" alt="">
						<div class="bottomContent">
							<h3>Rent a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>