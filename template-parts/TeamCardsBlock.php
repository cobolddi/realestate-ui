<section class="Section TeamCardsBlock">
	<div class="container">
		<div class="TopHeadingSec">
			<h2>Our Team</h2>
		</div>
		<div class="CardsBlock">
			<div class="row">
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempimg/team3.png" alt="">
						<div class="ContentBox">
							<h4>Aashish Bhardwaj</h4>
							<p>Property Management</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempimg/team2.png" alt="">
						<div class="ContentBox">
							<h4>Aashish Bhardwaj</h4>
							<p>Property Management</p>
						</div>
					</div>
				</div>
				<div class="col-12 col-md-4">
					<div class="Cards">
						<img src="assets/img/tempimg/team1.png" alt="">
						<div class="ContentBox">
							<h4>Kashish Bhardwaj</h4>
							<p>Property Management</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>