<section class="Section LeftImgRightContent">
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6">
				<div class="LeftImg">
					<img src="assets/img/tempimg/leftimg.png" alt="">
				</div>
			</div>
			<div class="col-12 col-md-6">
				<div class="RightContent">
					<h4 class="LeftYellowElement">About Us</h4>
					<h2>The leading real estate rental marketplace</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies. Pellentesque dictum sed libero non feugiat.</p>
					<p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
					<a href="#" class="OrangeWhiteBtn">Read More <span><svg><use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use></svg></span></a>
				</div>
			</div>
		</div>
	</div>
</section>