<section class="HomeBanner">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-8 IpadRemoved">
				<div class="VideoContainer js-videoWrapper">
					 <video src="assets/img/video-2.mp4" poster="assets/img/tempimg/bannerimg.png" class="videoIframe js-videoIframe"></video>
					 <button class="videoPoster js-videoPoster">
					 	<div class="text">Watch Videos</div>
					 	<div class="playbtn">
					 		<span>
					 			<img src="assets/img/play.svg" alt="">
					 		</span>
					 	</div>
					 	<!-- <img src="assets/img/arw-down.svg" alt="" class="play" /> -->
					 </button>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-4">
				<div class="BannerContent">
					<h4 class="LeftWhiteElement">Property Featured</h4>
					<h1>Creating modern Properties is our Specialty</h1>
					<p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit.</p>
					<a href="#" class="OrangeWhiteBtn">Contact us <span><svg><use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use></svg></span></a>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-lg-8 IpadDesktop">
				<div class="VideoContainer js-videoWrapper">
					 <video src="assets/img/video-2.mp4" poster="assets/img/tempimg/bannerimg.png" class="videoIframe js-videoIframe"></video>
					 <button class="videoPoster js-videoPoster">
					 	<div class="text">Watch Videos</div>
					 	<div class="playbtn">
					 		<span>
					 			<img src="assets/img/play.svg" alt="">
					 		</span>
					 	</div>
					 	<!-- <img src="assets/img/arw-down.svg" alt="" class="play" /> -->
					 </button>
				</div>
			</div>
		</div>
	</div>
</section>