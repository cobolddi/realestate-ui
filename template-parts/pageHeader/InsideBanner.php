<section class="InsideBanner">
	<picture>
		<source media="(min-width:650px)" srcset="assets/img/tempimg/insidebanner.svg">
		<source media="(min-width:465px)" srcset="assets/img/tempimg/insidebanner.svg">
		<img src="assets/img/tempimg/mobile-insidebanner.jpg" alt="Z-sites" style="width:auto;">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="content">
				<h1>About us</h1>
				<ul>
					<li>Home</li>
					<li><a href="#">About us</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>