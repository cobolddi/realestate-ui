<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Realestate-ui</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">
	
    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>
<body>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-6 col-md-3">
				<a href="index.php" class="logo">
					<img src="assets/img/logo.svg" alt="">
				</a>
			</div>
			<div class="col-6 col-md-9">
				<div class="IpadDesktop">
					<nav>
						<ul>
							<li>
								<a href="index.php">Home</a>
							</li>
							<li>
								<a href="about-us.php">About Us</a>
								
							</li>
							<li class="submenu">
								<a href="services.php">Service</a>
								<ul>
									<li>
										<a href="#">Buy a Home</a>				
									</li>
									<li>
										<a href="#">Sell a Home</a>				
									</li>
									<li>
										<a href="#">Rent a Home</a>				
									</li>
								</ul>
							</li>
							<li>
								<a href="news.php">News</a>
							</li>
							<li>
								<a href="contact-us.php">Contact</a>
							</li>
						</ul>
					</nav>
				</div>
				<div class="IpadRemoved">
					<div class="MobileMenu">
						<button class="c-hamburger c-hamburger--htx">
							<strong>Menu</strong>
					  		<span></span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>						
</header>


<nav class="sub-menu open">
	<ul>
		<li>
			<a href="index.php">Home</a>
		</li>
		<li>
			<a href="about-us.php">About Us</a>
		</li>
		<li class="haveSubmenu">
			<a href="services.php">Service</a>
			<ul>
				<li>
					<a href="#">Pizza</a>				
				</li>
				<li>
					<a href="#">Sandwich</a>				
				</li>
				<li>
					<a href="#">French Fries</a>				
				</li>
				<li>
					<a href="#">Cheeseburger</a>				
				</li>
				<li>
					<a href="#">Samosa</a>				
				</li>
			</ul>
		</li>
		<li>
			<a href="news.php">News</a>
		</li>
		<li>
			<a href="contact-us.php">Contact</a>
		</li>
	</ul>
</nav>

<main>