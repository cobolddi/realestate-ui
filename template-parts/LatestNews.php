<section class="Section LatestNewsSec">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-lg-3">
				<div class="Content">
					<h4 class="LeftYellowElement">News</h4>
					<h2>Latest News & Articles</h2>
					<p>We are very proud of the service we provide.</p>
					<a href="#" class="OrangeWhiteBtn">See All <span><svg><use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use></svg></span></a>
				</div>
			</div>
			<div class="col-12 col-lg-9">
				<div class="NewsSliderBlock">
					<div class="newslider">
					  	<div class="ArticleBlock">
					  		<a href="#" class="Cards">
								<img src="assets/img/tempimg/service-img.png" alt="">
								<div class="bottomContent">
									<h6 class="LeftBlueElement">March 8, 2021</h6>
									<h3>How to choose the Perfect Planner</h3>
									<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
									<ul>
										<li>By Admin /</li>
										<li>0 Comments</li>
									</ul>
								</div>
							</a>
					  	</div>
					  	<div class="ArticleBlock">
					  		<a href="#" class="Cards">
								<img src="assets/img/tempimg/service-img.png" alt="">
								<div class="bottomContent">
									<h6 class="LeftBlueElement">March 8, 2021</h6>
									<h3>How to choose the Perfect Planner</h3>
									<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
									<ul>
										<li>By Admin /</li>
										<li>0 Comments</li>
									</ul>
								</div>
							</a>
					  	</div>
					  	<div class="ArticleBlock">
					  		<a href="#" class="Cards">
								<img src="assets/img/tempimg/service-img.png" alt="">
								<div class="bottomContent">
									<h6 class="LeftBlueElement">March 8, 2021</h6>
									<h3>How to choose the Perfect Planner</h3>
									<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
									<ul>
										<li>By Admin /</li>
										<li>0 Comments</li>
									</ul>
								</div>
							</a>
					  	</div>
					  	<div class="ArticleBlock">
					  		<a href="#" class="Cards">
								<img src="assets/img/tempimg/service-img.png" alt="">
								<div class="bottomContent">
									<h6 class="LeftBlueElement">March 8, 2021</h6>
									<h3>How to choose the Perfect Planner</h3>
									<span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
									<ul>
										<li>By Admin /</li>
										<li>0 Comments</li>
									</ul>
								</div>
							</a>
					  	</div>	
					</div>
				</div>		
			</div>
		</div>
	</div>
</section>