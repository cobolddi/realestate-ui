<?php @include('template-parts/header.php') ?>

<section class="InsideBanner">
	<picture>
		<source media="(min-width:650px)" srcset="assets/img/tempimg/insidebanner.svg">
		<source media="(min-width:465px)" srcset="assets/img/tempimg/insidebanner.svg">
		<img src="assets/img/tempimg/mobile-insidebanner.jpg" alt="Z-sites" style="width:auto;">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="content">
				<h1>Services</h1>
				<ul>
					<li>Home</li>
					<li><a href="#">Services</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="Section ThreeCardsBlock ">
	<div class="container">
		<div class="TopHeadingSec">
			<h4 class="LeftYellowElement">Services</h4>
			<h2>Whether you’re buying, selling or renting, we can help you move forward.</h2>
			<p>We are very proud of the service we provide. See what our guests have to<br> say about us, our locations and services.</p>
		</div>
		<div class="ThreeCards">
			<div class="row">
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/service-img.png" alt="">
						<div class="bottomContent">
							<h3>Buy a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/bannerimg.png" alt="">
						<div class="bottomContent">
							<h3>Sell a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/leftimg.png" alt="">
						<div class="bottomContent">
							<h3>Rent a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/service-img.png" alt="">
						<div class="bottomContent">
							<h3>Buy a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/bannerimg.png" alt="">
						<div class="bottomContent">
							<h3>Sell a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/leftimg.png" alt="">
						<div class="bottomContent">
							<h3>Rent a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/service-img.png" alt="">
						<div class="bottomContent">
							<h3>Buy a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/bannerimg.png" alt="">
						<div class="bottomContent">
							<h3>Sell a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
				<div class="col-12 col-md-4">
					<a href="#" class="Cards">
						<img src="assets/img/tempimg/leftimg.png" alt="">
						<div class="bottomContent">
							<h3>Rent a Home</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
							<span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
						</div>
					</a>
				</div>
			</div>
		</div>
	</div>
</section>

<?php @include('template-parts/footer.php') ?>