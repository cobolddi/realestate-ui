<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Realestate-ui</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Automation of task with minification of css and js">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <link rel="icon" type="image/x-icon" href="assets/img/fav-icon.png">

    <link href="assets/css/vendor.min.css" rel="stylesheet">
    <link href="assets/css/styles.min.css" rel="stylesheet">
</head>

<body>
    <!-- <header id="header" class="Blueheader">
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-6 col-lg-3">
                    <a href="index.php" class="logo">
                        <img src="assets/img/white-logo.svg" alt="">
                    </a>
                </div>
                <div class="col-6 col-md-9">
                    <div class="IpadDesktop">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="about-us.php">About Us</a>

                                </li>
                                <li class="submenu">
                                    <a href="services.php">Service</a>
                                    <ul>
                                        <li>
                                            <a href="#">Buy a Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Sell a Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Rent a Home</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="news.php">News</a>
                                </li>
                                <li>
                                    <a href="contact-us.php">Contact</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="IpadRemoved">
                        <div class="MobileMenu">
                            <button class="c-hamburger c-hamburger--htx">
                                <strong>Menu</strong>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header> -->


    <header id="header">
        <div class="container">
            <div class="row">
                <div class="col-6 col-sm-6 col-lg-3">
                    <a href="index.php" class="logo">
                        <img src="assets/img/logo.svg" alt="">
                    </a>
                </div>
                <div class="col-6 col-sm-6 col-lg-9">
                    <div class="IpadDesktop">
                        <nav>
                            <ul>
                                <li>
                                    <a href="index.php">Home</a>
                                </li>
                                <li>
                                    <a href="about-us.php">About Us</a>

                                </li>
                                <li class="submenu">
                                    <a href="services.php">Service</a>
                                    <ul>
                                        <li>
                                            <a href="#">Buy a Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Sell a Home</a>
                                        </li>
                                        <li>
                                            <a href="#">Rent a Home</a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="news.php">News</a>
                                </li>

                                <a href="#" class="OrangeWhiteBtn">Contact us <span><svg>
                                            <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                        </svg></span></a>
                            </ul>
                        </nav>
                    </div>
                    <div class="IpadRemoved">
                        <div class="MobileMenu">
                            <button class="c-hamburger c-hamburger--htx">
                                <strong>Menu</strong>
                                <span></span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <nav class="sub-menu open">
        <ul>
            <li>
                <a href="index.php">Home</a>
            </li>
            <li>
                <a href="about-us.php">About Us</a>
            </li>
            <li class="haveSubmenu">
                <a href="our-menu.php">Service</a>
                <ul>
                    <li>
                        <a href="#">Pizza</a>
                    </li>
                    <li>
                        <a href="#">Sandwich</a>
                    </li>
                    <li>
                        <a href="#">French Fries</a>
                    </li>
                    <li>
                        <a href="#">Cheeseburger</a>
                    </li>
                    <li>
                        <a href="#">Samosa</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="news.php">News</a>
            </li>
            <li>
                <a href="contact-us.php">Contact</a>
            </li>
        </ul>
    </nav>

    <main>

        <section class="noBanner"></section>

        <section class="HomeBanner">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-8 IpadRemoved">
                        <div class="VideoContainer js-videoWrapper">
                            <video src="assets/img/video-2.mp4" poster="assets/img/tempimg/bannerimg.png"
                                class="videoIframe js-videoIframe"></video>

                            <span class="bottomYellowBorder"></span>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-4">
                        <div class="BannerContent">
                            <h4 class="LeftWhiteElement">Property Featured</h4>
                            <h1>Creating modern Properties is our Specialty</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit.</p>
                            <a href="#" class="OrangeWhiteBtn">Contact us <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-8 IpadDesktop">
                        <div class="VideoContainer js-videoWrapper">
                            <video src="assets/img/video-2.mp4" poster="assets/img/tempimg/bannerimg.png"
                                class="videoIframe js-videoIframe"></video>
                            <span class="bottomYellowBorder"></span>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section ModernProperty">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-12">
                        <div class="BannerContent">
                            <h4 class="LeftWhiteElement RightWhiteElement">Property Featured</h4>
                            <h1>Creating modern Properties is our Specialty</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit.</p>
                            <a href="#" class="OrangeWhiteBtn">Contact us <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="HomeBanner ComponentSecTwo">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div class="BannerContent">
                            <h4 class="LeftWhiteElement">Property Featured</h4>
                            <h1>Creating modern Properties is our Specialty</h1>
                            <p>Lorem ipsum dolor sit amet, consectetur <br>adipiscing elit.</p>
                            <a href="#" class="OrangeWhiteBtn">Contact us <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-lg-6">
                        <div>
                            <img src="assets/img/components/SecTwoRightImage.png" width="100%" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <?php @include('template-parts/pageHeader/InsideBanner.php') ?>


        <section class="Section LeftImgRightContent">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="LeftImg">
                            <img src="assets/img/tempimg/leftimg.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Right content left image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="LeftImgRightContent">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 IpadRemoved">
                        <div class="LeftImg">
                            <img src="assets/img/tempimg/leftimg.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Left content right image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 IpadDesktop">
                        <div class="LeftImg">
                            <img src="assets/img/tempimg/leftimg.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section LeftImgRightContent hideAfterBefore">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="LeftImg">
                            <img src="assets/img/components/rightImg3.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Right content left image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="LeftImgRightContent hideAfterBefore">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6 IpadRemoved">
                        <div class="LeftImg">
                            <img src="assets/img/components/img-4.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Left content right image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 IpadDesktop">
                        <div class="LeftImg">
                            <img src="assets/img/components/img-4.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section LeftImgRightContent hideAfterBefore">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="LeftImg">
                            <img src="assets/img/components/img-5.png" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Right content left image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="LeftImgRightContent hideAfterBefore">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Right content left image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h4 class="LeftYellowElement">Right content left image</h4>
                            <h2>The leading real estate rental marketplace</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec augue quam, vulputate in
                                laoreet quis, tempus non diam. Donec placerat quam vel purus semper ultricies.
                                Pellentesque dictum sed libero non feugiat.</p>
                            <p>Ut in lacus sit amet risus convallis sollicitudin. Nam pulvinar elementum commodo. Ut
                                tortor sapien, tempus in tincidunt sit amet, pulvinar non justo.</p>
                            <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php @include('template-parts/ClientSlider.php') ?>


        <section class="TypographySection">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4">
                        <div class="RightContent">
                            <h1 class="hOneTypo mt-0">Typography</h1>
                            <h1 class="hOneTypo">H1 A Visual Type Scale</h1>
                            <h2 class="hTwoTypo">H2 A Visual Type Scale</h2>
                            <h3 class="hThreeTypo">H3 A Visual Type Scale</h3>
                            <h4 class="hFourTypo">H4 A Visual Type Scale</h4>
                        </div>
                    </div>
                    <div class="col-12 col-md-8">
                        <div class="RightContent d-f">
                            <div>
                                <h2 class="hTwoTypo">Primary button</h2>
                                <a href="#" class="OrangeWhiteBtn">Read More <span><svg>
                                            <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                        </svg></span></a>
                            </div>
                            <div>
                                <h2 class="hTwoTypo">Secondary button</h2>
                                <a href="#" class="OrangeWhiteBtn SecondaryBtn">Read More <span><svg>
                                            <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                        </svg></span></a>
                            </div>
                            <button aria-label="Previous" type="button" style=""><img src="assets/img/arrow-left.svg"
                                    alt=""></button>
                            <button aria-label="Next" type="button" style=""><img src="assets/img/arrow.svg"
                                    alt=""></button>
                        </div>
                        <div class="RightContent mt-50">
                            <a href="#" class="ReadMoreYellow">
                                <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                            </a>
                            <a href="#" class="ReadMoreYellow ReadMoreBlue">
                                <span>Read More <img src="assets/img/blue-arw.svg" alt=""></span>
                            </a>

                        </div>
                        <div class="RightContent mt-50">
                            <h2 class="hTwoTypo">Text Field</h2>
                            <div class="d-f"><input type="text" placeholder="Full Name">
                                <select name="" id="">
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                    <option value="">Subject</option>
                                </select></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="Section ThreeCardsBlock FourCardsSec">
            <div class="container">
                <h2 class="TitleHeading">Four Cards</h2>
                <div class="ThreeCards">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/service-img.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/bannerimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/leftimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/service-img.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="ThreeCardsBlock FourCardsSec">
            <div class="container">
                <div class="ThreeCards">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/service-img.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/bannerimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/leftimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/service-img.png" alt="">
                                <div class="bottomContent">
                                    <h3>Minimalist Style For Good Life</h3>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="FourCardsBlock">
            <div class="container">
                <div class="CardsBlock">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards">
                                <div class="IconBox">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#transparent"></use>
                                    </svg>
                                </div>
                                <div class="ContentBox">
                                    <h5>Transparency</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards">
                                <div class="IconBox">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#expert"></use>
                                    </svg>
                                </div>
                                <div class="ContentBox">
                                    <h5>Expertise</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards">
                                <div class="IconBox">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#integrity"></use>
                                    </svg>
                                </div>
                                <div class="ContentBox">
                                    <h5>Integrity</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards">
                                <div class="IconBox">
                                    <svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#respect"></use>
                                    </svg>
                                </div>
                                <div class="ContentBox">
                                    <h5>Respect</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="FourCardsBlock">
            <div class="container">
                <div class="CardsBlock">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards WithoutIcon">
                                <div class="ContentBox">
                                    <h5>Transparency</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards WithoutIcon">
                                <div class="ContentBox">
                                    <h5>Expertise</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards WithoutIcon">
                                <div class="ContentBox">
                                    <h5>Integrity</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-3">
                            <div class="Cards WithoutIcon">
                                <div class="ContentBox">
                                    <h5>Respect</h5>
                                    <p>There are many new variations of pasages of available text.</p>
                                    <a href="#" class="ReadMoreYellow">
                                        <span>know More <img src="assets/img/orange-arw.svg" alt=""></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section ThreeCardsBlock ">
            <div class="container">
                <h2 class="TitleHeading">Services Cards</h2>
                <div class="ThreeCards">
                    <div class="row">
                        <div class="col-12 col-md-4">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/service-img.png" alt="">
                                <div class="bottomContent">
                                    <h3>Buy a Home</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/bannerimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Sell a Home</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                        <div class="col-12 col-md-4">
                            <a href="#" class="Cards">
                                <img src="assets/img/tempimg/leftimg.png" alt="">
                                <div class="bottomContent">
                                    <h3>Rent a Home</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                    <span>know More <img src="assets/img/orange-arw.svg" alt=""></span>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="PropertyDetails">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="SliderBlock">
                            <div class="main">
                                <div class="slider slider-for">
                                    <div><img src="assets/img/tempimg/dummy.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/dummy.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/dummy.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/dummy.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/dummy.png" alt=""></div>
                                </div>
                                <div class="slider slider-nav">
                                    <div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
                                    <div><img src="assets/img/tempimg/thumbnail.png" alt=""></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="RightContent">
                            <h2>Property Description</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est
                                accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis
                                turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper
                                massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                                consectetur at luctus sit amet, viverra mollis leo.</p>
                            <p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est
                                accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget, iaculis
                                turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar semper
                                massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam tellus augue,
                                consectetur at luctus sit amet, viverra mollis leo.</p>
                            <a href="#test-popup" class="OrangeWhiteBtn open-popup-link">Contact us <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                            <div id="test-popup" class="white-popup mfp-hide">
                                <h4 class="LeftYellowElement">Contact Us</h4>
                                <h2>Fill the form to contact us.</h2>
                                <form action="">
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <input type="text" placeholder="Full Name">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <input type="email" placeholder="E-mail">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <input type="text" placeholder="Phone">
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <input type="text" placeholder="Subject">
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <textarea placeholder="Message"></textarea>
                                        </div>
                                        <div class="col-12 col-md-12">
                                            <div class="submit"><input type="submit" value="Submit"></div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section LiteBlueSection OurGallery">
            <div class="container">
                <div class="TopHeading">
                    <h2 class="PinkElement">Gallery Section</h2>
                </div>
                <div class="GalleryBlock">
                    <div class="row">
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news1.png">
                                <img src="assets/img/tempimg/news1.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news2.png">
                                <img src="assets/img/tempimg/news2.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news3.png">
                                <img src="assets/img/tempimg/news3.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news4.png">
                                <img src="assets/img/tempimg/news4.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news2.png">
                                <img src="assets/img/tempimg/news2.png" />
                            </a>
                        </div>
                        <div class="col-6 col-md-4">
                            <a class="image-popup-vertical-fit" href="assets/img/tempimg/news3.png">
                                <img src="assets/img/tempimg/news3.png" />
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="TestimonialsSec">
            <div class="container">
                <h2 class="TitleHeading">Client feedback</h2>
                <div class="testimonialslider">
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                    <div class="SlideBlock">
                        <div class="author"><img src="assets/img/tempimg/author.png" alt=""></div>
                        <h3>This is due to their excellent service, Competitive pricing and customer support. It’s
                            throughly refreshing to get such a personal touch.</h3>
                        <p>Uday Handa / <span>Founder</span></p>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section faq-sec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="TitleHeading">Frequently Asked Questions</h2>
                    </div>
                </div>
                <div class="accordion">

                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                    <div class="accordion-item">
                        <button id="accordion-button-1" aria-expanded="false">
                            <span class="accordion-title">Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit?</span>
                            <span class="icon" aria-hidden="true">
                                <img src="assets/img/accordian-arrow.svg" alt="">
                            </span>
                        </button>
                        <div class="accordion-content">
                            <p>
                                Taking part in our unique competition is a chance to challenge yourself/your
                                team and exhibit what you can do. Doing is the best way to learn new skills and
                                tools. You also get to grow your design portfolio, win cash awards and
                                certificates.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="VideoBlockSection">
            <div class="container">
                <h2 class="TitleHeading center">Intro Video section</h2>
                <div class="VideoBlock">
                    <img src="assets/img/tempimg/video.png" alt="">
                    <a href="https://www.youtube.com/watch?v=bY-mOdgz7zQ" class="PlayBtn"><img
                            src="assets/img/play-white.svg" alt=""></a>
                </div>
            </div>
        </section>

        <section class="Section LatestNewsSec BlogSec ">
            <div class="container">
                <h2 class="TitleHeading">Blog Listing Card</h2>
                <div class="row">
                    <div class="col-12 col-sm-12 col-lg-3">
                        <div class="Content">
                            <h4 class="LeftYellowElement">Blog</h4>
                            <h2>Latest Blogs</h2>
                            <p>We are very proud of the service we provide.</p>
                            <a href="#" class="OrangeWhiteBtn">See All <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="NewsSliderBlock">
                            <div class="newslider">
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row rowmt80">
                    <div class="col-12 col-sm-12 col-lg-3">
                        <div class="Content">
                            <h4 class="LeftYellowElement">Blog</h4>
                            <h2>Latest Blogs</h2>
                            <p>We are very proud of the service we provide.</p>
                            <a href="#" class="OrangeWhiteBtn">See All <span><svg>
                                        <use xlink:href="assets/img/cobold-sprite.svg#drkarw"></use>
                                    </svg></span></a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-9">
                        <div class="NewsSliderBlock">
                            <div class="newslider">
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                                <div class="ArticleBlock">
                                    <a href="#" class="Cards">
                                        <img src="assets/img/tempimg/service-img.png" alt="">
                                        <div class="bottomContent">
                                            <h6 class="LeftBlueElement">March 8, 2021</h6>
                                            <h3>How to choose the Perfect Planner</h3>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                                            <span>Read More <img src="assets/img/orange-arw.svg" alt=""></span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section BlogDetails">
            <div class="container">
                <h2 class="TitleHeading">Blog Detail</h2>
                <div class="BlogCardsBlock">
                    <div class="row">
                        <div class="col-12 col-md-12">
                            <img src="assets/img/components/blog-details.png" alt="Blog Details Image">
                            <div class="BlogContent SmallContainer">
                                <h2 class="TitleHeading">What Makes Engineering Consulting Different</h2>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eu est in est
                                    accumsan malesuada at sed ipsum. Maecenas eget quam aliquam, finibus lacus eget,
                                    iaculis turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut pulvinar
                                    semper massa a varius. Nulla pulvinar lacus non mi ullamcorper laoreet. Aliquam
                                    tellus augue, consectetur at luctus sit amet, viverra mollis leo. Fusce sed velit
                                    eget purus volutpat aliquet nec nec lacus. Orci varius natoque penatibus et magnis
                                    dis parturient montes, nascetur ridiculus mus. Pellentesque quam libero, consequat
                                    non dolor eu, egestas venenatis nunc. Praesent sit amet lorem ultricies, pulvinar
                                    sapien sodales, tempor dui. Nam posuere tortor nec nisl rhoncus mattis. Mauris
                                    sagittis magna quis neque feugiat dignissim.<br><br> Integer id ornare turpis.
                                    Vestibulum neque purus, tempus placerat pretium ut, ullamcorper at purus.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <section class="sec-16 TabSec">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 wrapper">
                        <div class="tab_btn--wrapper">
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-1" class="btn-tab active">Tab
                                1</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-2" class="btn-tab">Tab
                                2</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-3" class="btn-tab">Tab
                                3</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-4" class="btn-tab">Tab
                                4</button>
                            <button data-toggle="tab" data-tabs=".gtabs" data-tab=".tab-5" class="btn-tab">Tab
                                5</button>
                        </div>
                        <div class="gtabs">
                            <div class="gtab active tab-1">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Phasellus malesuada sapien vitae scelerisque facilisis. Curabitur elementum eget
                                    risus nec rhoncus. Maecenas et consequat nibh, in porttitor ex. Ut congue purus ac
                                    ornare vehicula. Phasellus eu sodales dolor. Praesent commodo sem eget accumsan
                                    vestibulum. Praesent fringilla at lorem vitae viverra.
                                </p>
                                <div class="lists">
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                    <p class="list">
                                        <span class="subcopy">Lorem ipsum dolor sit amet, consectetur adipiscing
                                            elit.</span>
                                    </p>
                                </div>
                            </div>
                            <div class="gtab tab-2">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>
                            <div class="gtab tab-3">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>

                            <div class="gtab tab-4">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>

                            <div class="gtab tab-5">
                                <h2 class="sub_heading TitleHeading">TabName</h2>
                                <p class="subcopy">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipiscelit. Nisi minima fugit, est facere molestiae quod pariatur.
                                    Consectetur natus, blanditiis laborum possimus doloremque harum adipisci debitis
                                    similique, nostrum provident ut dolelit. Nisi minima fugit, est facere molestiae
                                    quod pariatur. Consectetur natus, blanditiis laborum possimus doloremque harum
                                    adipisci debitis similique, nostrum provident ut dolelit. Nisi minima fugit, est
                                    facere molestiae quod pariatur. Consectetur natus, blanditiis laborum possimus
                                    doloremque harum adipisci debitis similique, nostrum provident ut doli debitis
                                    similique, nostrum provident ut dolore.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section FormDropdownelement">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-7">
                        <h2 class="TitleHeading">Booking form</h2>
                        <div class="FormBlock">
                            <h4 class="LeftYellowElement">Contact Us</h4>
                            <h2>Fill the form to contact us.</h2>
                            <form action="">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Full Name">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="email" placeholder="E-mail">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Phone">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Subject">
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <textarea placeholder="Message"></textarea>
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <div class="submit"><input type="submit" value="Submit"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <h2 class="TitleHeading">Dropdown menu</h2>
                        <ul class="dropdown">
                            <li>
                                <a href="#">Buy a Home</a>
                            </li>
                            <li>
                                <a href="#">Sell a Home</a>
                            </li>
                            <li>
                                <a href="#">Rent a Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>


        <section class="ContactFormBlock">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="AddressBlock">
                            <span class="icon">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#add"></use>
                                </svg>
                            </span>
                            <h4>South Delhi, India <br><span>Thinkvalley B13, Sector32, Gurgaon - 122002, Haryana
                                    (India)</span></h4>
                        </div>

                        <div class="AddressBlock">
                            <span class="icon">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#mail"></use>
                                </svg>
                            </span>
                            <h4>Email <br><a href="mailto:hi@cobold.in">hi@cobold.in</a></h4>
                        </div>

                        <div class="AddressBlock">
                            <span class="icon">
                                <svg>
                                    <use xlink:href="assets/img/cobold-sprite.svg#phone"></use>
                                </svg>
                            </span>
                            <h4>Phone <br><a href="tel:+91-981079XXXX">+91 - 981079XXXX</a></h4>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-8">
                        <div class="FormBlock">
                            <h2>Get in Touch</h2>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            <form action="">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Full Name">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="email" placeholder="E-mail">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Phone">
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <input type="text" placeholder="Subject">
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <textarea placeholder="Message"></textarea>
                                    </div>
                                    <div class="col-12 col-md-12">
                                        <div class="submit"><input type="submit" value="Submit"></div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section MapSec">
            <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3506.6254739568285!2d77.14354531446284!3d28.490823697232504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1e23b39383f5%3A0xf03adb9a3b5fa5a!2sCobold%20Digital!5e0!3m2!1sen!2sin!4v1618915905557!5m2!1sen!2sin"
                width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </section>
        <?php @include('template-parts/footer.php') ?>