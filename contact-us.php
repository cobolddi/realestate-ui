<?php @include('template-parts/header.php') ?>

<section class="InsideBanner">
	<picture>
		<source media="(min-width:650px)" srcset="assets/img/tempimg/insidebanner.svg">
		<source media="(min-width:465px)" srcset="assets/img/tempimg/insidebanner.svg">
		<img src="assets/img/tempimg/mobile-insidebanner.jpg" alt="Z-sites" style="width:auto;">
	</picture>
	<div class="BannerContent">
		<div class="container">
			<div class="content">
				<h1>Contact us</h1>
				<ul>
					<li>Home</li>
					<li><a href="#">Contact us</a></li>
				</ul>
			</div>
		</div>
	</div>
</section>

<section class="Section ContactFormBlock">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-6 col-lg-4">
				<div class="AddressBlock">
					<span class="icon">
						<svg>
							<use xlink:href="assets/img/cobold-sprite.svg#add"></use>
						</svg>
					</span>
					<h4>South Delhi, India <br><span>Thinkvalley B13, Sector32, Gurgaon - 122002, Haryana (India)</span></h4>
				</div>
				
				<div class="AddressBlock">
					<span class="icon">
						<svg>
							<use xlink:href="assets/img/cobold-sprite.svg#mail"></use>
						</svg>
					</span>
					<h4>Email <br><a href="mailto:hi@cobold.in">hi@cobold.in</a></h4>
				</div>

				<div class="AddressBlock">
					<span class="icon">
						<svg>
							<use xlink:href="assets/img/cobold-sprite.svg#phone"></use>
						</svg>
					</span>
					<h4>Phone <br><a href="tel:+91-981079XXXX">+91 - 981079XXXX</a></h4>
				</div>
			</div>
			<div class="col-12 col-sm-6 col-lg-8">
				<div class="FormBlock">
					<h2>Get in Touch</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					<form action="">
						<div class="row">
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Full Name">
							</div>
							<div class="col-12 col-md-6">
								<input type="email" placeholder="E-mail">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Phone">
							</div>
							<div class="col-12 col-md-6">
								<input type="text" placeholder="Subject">
							</div>
							<div class="col-12 col-md-12">
								<textarea placeholder="Message"></textarea>
							</div>
							<div class="col-12 col-md-12">
								<div class="submit"><input type="submit" value="Submit"></div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>		
</section>

<section>
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3506.6254739568285!2d77.14354531446284!3d28.490823697232504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1e23b39383f5%3A0xf03adb9a3b5fa5a!2sCobold%20Digital!5e0!3m2!1sen!2sin!4v1618915905557!5m2!1sen!2sin" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
</section>

<?php @include('template-parts/footer.php') ?>